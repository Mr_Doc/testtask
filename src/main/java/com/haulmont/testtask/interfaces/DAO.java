package com.haulmont.testtask.interfaces;

import lombok.NonNull;

import java.util.List;

public interface DAO<Entity, Key> {

    boolean create(@NonNull final Entity entity);

    Entity read(@NonNull final Key id);

    boolean update(@NonNull final Entity entity);

    boolean delete(@NonNull final Entity entity);

    List<Entity> getAll();
}
