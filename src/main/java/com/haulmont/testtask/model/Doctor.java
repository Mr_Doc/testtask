package com.haulmont.testtask.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    @NonNull
    @Pattern(regexp="([А-Я]+)")
    @Column(name = "Surname", nullable = false)
    private String surname;

    @NonNull
    @Pattern(regexp="([А-Я]+)")
    @Column(name = "Name", nullable = false)
    private String name;

    @Pattern(regexp="([А-Я]+)")
    @Column(name = "Patronymic")
    private String patronymic;

    @NonNull
    @Pattern(regexp="([А-Я]+)")
    @Column(name = "Specialization")
    private String specialization;

    @OneToMany(mappedBy = "doctor")
    private List<Recipe> recipes;

    public void setSurname(String surname) {
        this.surname = surname.toUpperCase();
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic.toUpperCase();
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization.toUpperCase();
    }

    @Override
    public String toString() {
        return surname + " " + name + " " + patronymic;
    }
}
