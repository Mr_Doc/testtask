package com.haulmont.testtask.model;

public enum Priority {
    NORMAL("НОРМАЛЬНЫЙ"),
    CITO("СРОЧНЫЙ"),
    STATIM("НЕМЕДЛЕННЫЙ");

    private String priority;

    Priority(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return this.priority;
    }

    @Override
    public String toString() {
        return this.priority;
    }
}
