package com.haulmont.testtask.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Patient")
public class Patient {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(unique = true)
     private Long id;

     @NonNull
     @Pattern(regexp="([А-Я]+)")
     @Column(name = "Surname", nullable = false)
     private String surname;

     @NonNull
     @Pattern(regexp="([А-Я]+)")
     @Column(name = "Name", nullable = false)
     private String name;

     @Pattern(regexp="([А-Я]+)")
     @Column(name = "Patronymic")
     private String patronymic;

     @NonNull
     @Pattern(regexp="([8][0-9]{10})")
     @Column(name = "Phone_Number")
     private String phoneNumber;

     @OneToMany(mappedBy = "patient")
     private List<Recipe> recipes;

     public void setSurname(String surname) {
          this.surname = surname.toUpperCase();
     }

     public void setName(String name) {
          this.name = name.toUpperCase();
     }

     public void setPatronymic(String patronymic) {
          this.patronymic = patronymic.toUpperCase();
     }

     @Override
     public String toString() {
          return surname + " " + name + " " + patronymic;
     }

     @Override
     public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || getClass() != o.getClass()) return false;
          Patient patient = (Patient) o;
          return Objects.equals(id, patient.id) &&
                  Objects.equals(surname, patient.surname) &&
                  Objects.equals(name, patient.name) &&
                  Objects.equals(patronymic, patient.patronymic) &&
                  Objects.equals(phoneNumber, patient.phoneNumber);
     }

     @Override
     public int hashCode() {
          return Objects.hash(id, surname, name, patronymic, phoneNumber);
     }
}
