package com.haulmont.testtask.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.print.Doc;
import javax.validation.constraints.Pattern;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    @NonNull
    @Column(name = "description")
    private String description;

    @NonNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @NonNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @NonNull
    @Column(name = "dateOfCreation")
    private LocalDate dateOfCreation;

    @NonNull
    @Column(name = "expirationDate")
    private LocalDate expirationDate;

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(name = "priority")
    private Priority priority;

    public void setDescription(String description) {
        this.description = description.toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return Objects.equals(id, recipe.id) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(dateOfCreation, recipe.dateOfCreation) &&
                Objects.equals(expirationDate, recipe.expirationDate) &&
                priority == recipe.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, dateOfCreation, expirationDate, priority);
    }
}
