package com.haulmont.testtask.dao;

import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Patient;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class PatientDAO implements DAO<Patient, Long> {

    private final SessionFactory factory;

    public PatientDAO(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public boolean create(@NonNull Patient patient) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(patient);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public Patient read(@NonNull Long id) {
        try (final Session session = factory.openSession()) {
            return session.get(Patient.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(@NonNull Patient patient) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(patient);
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(@NonNull Patient patient) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(patient);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Patient> getAll() {
        try (final Session session = factory.openSession()) {
            Query query = session.createQuery("from Patient");
            List<Patient> list = (List<Patient>) query.getResultList();
            session.close();
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }
}
