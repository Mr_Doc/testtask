package com.haulmont.testtask.dao;

import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Recipe;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class RecipeDAO implements DAO<Recipe, Long> {

    private final SessionFactory factory;

    public RecipeDAO(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public boolean create(@NonNull Recipe recipe) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(recipe);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public Recipe read(@NonNull Long id) {
        try (final Session session = factory.openSession()) {
            return session.get(Recipe.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(@NonNull Recipe recipe) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(recipe);
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(@NonNull Recipe recipe) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(recipe);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Recipe> getAll() {
        try (final Session session = factory.openSession()) {
            Query query = session.createQuery("from Recipe ");
            List<Recipe> list = (List<Recipe>) query.getResultList();
            session.close();
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }
}
