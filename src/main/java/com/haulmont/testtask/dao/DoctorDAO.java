package com.haulmont.testtask.dao;

import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Doctor;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class DoctorDAO implements DAO<Doctor, Long> {

    private final SessionFactory factory;

    public DoctorDAO(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public boolean create(@NonNull Doctor doctor) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(doctor);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public Doctor read(@NonNull Long id) {
        try (final Session session = factory.openSession()) {
            return session.get(Doctor.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(@NonNull Doctor doctor) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(doctor);
            session.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(@NonNull Doctor doctor) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(doctor);
            session.getTransaction().commit();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Doctor> getAll() {
        try (final Session session = factory.openSession()) {
            Query query = session.createQuery("from Doctor");
            List<Doctor> list = (List<Doctor>) query.getResultList();
            session.close();
            return list;
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }

}
