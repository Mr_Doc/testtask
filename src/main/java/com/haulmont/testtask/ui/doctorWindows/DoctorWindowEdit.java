package com.haulmont.testtask.ui.doctorWindows;

import com.haulmont.testtask.dao.DoctorDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Doctor;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;



public class DoctorWindowEdit extends Window {

    private TextField surnameTextField = new TextField("Фамилия");
    private TextField nameTextField = new TextField("Имя");
    private TextField patronymicTextField = new TextField("Отчество");
    private TextField specializationTextField = new TextField("Специализация");
    private Binder<Doctor> binder = new Binder<>();
    private DoctorWindowGrid previousWindow;
    private Doctor selectedDoctor;

    public DoctorWindowEdit(String pressedButton, Doctor selectedDoctor, DoctorWindowGrid previousWindow) {
        this.selectedDoctor = selectedDoctor;
        this.previousWindow = previousWindow;
        VerticalLayout layout = createVerticalLayout(pressedButton);
        initBinder(pressedButton);
        setContent(layout);
    }

    private VerticalLayout createVerticalLayout(String button) {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(surnameTextField);
        layout.addComponent(nameTextField);
        layout.addComponent(patronymicTextField);
        layout.addComponent(specializationTextField);
        layout.addComponent(getButtonLayout(button));
        return layout;
    }

    private void initBinder(String pressedButton) {
        if (pressedButton.equals("edit")) {
            setBinder();
            addValues(selectedDoctor);
        }
    }

    private void setBinder() {
        binder.bind(surnameTextField, Doctor::getSurname, Doctor::setSurname);
        binder.bind(nameTextField, Doctor::getName, Doctor::setName);
        binder.bind(patronymicTextField, Doctor::getPatronymic, Doctor::setPatronymic);
        binder.bind(specializationTextField, Doctor::getSpecialization, Doctor::setSpecialization);
    }

    private void addValues(Doctor doctor) {
        binder.readBean(doctor);
    }

    private HorizontalLayout getButtonLayout(String button) {
        HorizontalLayout layout = new HorizontalLayout();
        Button okButton = createOKButton(button);
        Button undoButton = createUNDOButton();
        layout.addComponent(okButton);
        layout.addComponent(undoButton);
        return layout;
    }

    private Button createOKButton(String button) {
        Button okButton = new Button("ОК");
        okButton.addClickListener(clickEvent -> {
            if (binder.isValid()) {
                if (button.equals("edit")) {
                    try {
                        updateDoctor();
                    } catch (ValidationException e) {
                        e.printStackTrace();
                    }
                } else {
                    createDoctor();
                }
                previousWindow.updateDataDoctor();
            }
        });
        return okButton;
    }

    private Button createUNDOButton() {
        Button undoButton = new Button("Отменить");
        undoButton.addClickListener(clickEvent -> this.close());
        return undoButton;
    }


    private void updateDoctor() throws ValidationException {
        DAO<Doctor, Long> dao = new DoctorDAO(MainUI.SESSION_FACTORY);
        binder.writeBean(selectedDoctor);
        if (!dao.update(selectedDoctor)){
            Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
        } else {
            Notification.show("Данные обновлены", Notification.Type.TRAY_NOTIFICATION);
            this.close();
        }
    }

    private void createDoctor() {
        try {
            DAO<Doctor, Long> dao = new DoctorDAO(MainUI.SESSION_FACTORY);
            Doctor doctor = new Doctor();
            doctor.setId(1L);
            doctor.setSurname(surnameTextField.getValue());
            doctor.setName(nameTextField.getValue());
            doctor.setPatronymic(patronymicTextField.getValue());
            doctor.setSpecialization(specializationTextField.getValue());
            if (!dao.create(doctor)) {
                Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
            } else {
                Notification.show("Создано", Notification.Type.TRAY_NOTIFICATION);
                this.close();
            }
        } catch (NullPointerException exception){
            Notification.show("Все поля обязательны к заполнению", Notification.Type.ERROR_MESSAGE);
        }

    }
}
