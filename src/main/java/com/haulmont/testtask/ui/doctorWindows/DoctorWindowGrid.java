package com.haulmont.testtask.ui.doctorWindows;

import com.haulmont.testtask.dao.DoctorDAO;
import com.haulmont.testtask.dao.RecipeDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Doctor;
import com.haulmont.testtask.model.Recipe;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.ui.*;

import java.util.List;

public class DoctorWindowGrid extends Window {

    private Doctor selectedDoctor = null;
    private Grid<Doctor> grid = new Grid<>();
    private VerticalLayout mainLayout = new VerticalLayout();
    private MainUI mainUI;
    private DAO<Doctor,Long> dao = new DoctorDAO(MainUI.SESSION_FACTORY);

    public DoctorWindowGrid(MainUI mainUI) {
        this.mainUI = mainUI;
        initGrid();
        updateDataDoctor();
        mainLayout.addComponent(createMenuButtonsLayout());
        mainLayout.addComponent(getButtonLayout());
        mainLayout.addComponentsAndExpand(grid);
        this.setContent(mainLayout);
    }

    public VerticalLayout getLayout() {
        return mainLayout;
    }

    private void initGrid() {
        grid.addColumn(Doctor::getSurname).setCaption("Фамилия");
        grid.addColumn(Doctor::getName).setCaption("Имя");
        grid.addColumn(Doctor::getPatronymic).setCaption("Отчество");
        grid.addColumn(Doctor::getSpecialization).setCaption("Специализация");
        grid.setVisible(true);
        grid.setSizeFull();
        grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> selectedDoctor = valueChangeEvent.getValue());
    }

    public void updateDataDoctor() {
        List<Doctor> list = dao.getAll();
        grid.setItems(list);
    }

    private HorizontalLayout createMenuButtonsLayout() {
        return mainUI.createMenuHorizontalLayout();
    }

    private HorizontalLayout getButtonLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        Button addButton = createAddButton();
        Button editButton = createEditButton();
        Button deleteButton = createDeleteButton();
        Button recipeButton = createRecipeAmountButton();
        layout.addComponent(addButton);
        layout.addComponent(editButton);
        layout.addComponent(deleteButton);
        layout.addComponent(recipeButton);
        return layout;
    }

    private Button createAddButton() {
        Button addButton = new Button("Добавить");
        addButton.addClickListener(e -> {
            DoctorWindowEdit window = new DoctorWindowEdit("add", null, this);
            setDefaultWindowSettings(window);
            UI.getCurrent().addWindow(window);
        });
        return addButton;
    }

    private Button createEditButton() {
        Button editButton = new Button("Изменить");
        editButton.addClickListener(clickEvent -> {
            if (selectedDoctor != null) {
                DoctorWindowEdit window = new DoctorWindowEdit("edit", selectedDoctor, this);
                setDefaultWindowSettings(window);
                UI.getCurrent().addWindow(window);
            } else {
                Notification.show("Выберите строку для её изменения", Notification.Type.WARNING_MESSAGE);
            }
        });
        return editButton;
    }

    private Button createDeleteButton() {
        Button deleteButton = new Button("Удалить");
        deleteButton.addClickListener(clickEvent -> {
            if (selectedDoctor != null) {
                if (dao.delete(selectedDoctor)){
                    selectedDoctor = null;
                    updateDataDoctor();
                    Notification.show("Удалено", Notification.Type.TRAY_NOTIFICATION);
                } else {
                    Notification.show("Невозможно удалить:существует рецепт", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                Notification.show("Выберите строку для её удаления", Notification.Type.WARNING_MESSAGE);
            }
        });
        return deleteButton;
    }

    private Button createRecipeAmountButton() {
        Button amountRecipeButton = new Button("Статистика");
        amountRecipeButton.addClickListener(clickEvent -> {
            DAO<Recipe, Long> dao = new RecipeDAO(MainUI.SESSION_FACTORY);
            Notification.show("Выписано рецептов - " + dao.getAll().size());
        });
        return amountRecipeButton;
    }

    private void setDefaultWindowSettings(Window window) {
        window.setPosition(800, 200);
        window.setResizable(false);
        window.setWidth("300px");
        window.setHeight("400px");
    }
}
