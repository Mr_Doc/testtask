package com.haulmont.testtask.ui.recipeWindows;

import com.haulmont.testtask.dao.RecipeDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Doctor;
import com.haulmont.testtask.model.Patient;
import com.haulmont.testtask.model.Priority;
import com.haulmont.testtask.model.Recipe;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;

import java.util.List;


public class RecipeWindowEdit extends Window {

    private TextField descriptionField = new TextField("Описание");
    private ComboBox<Patient> patientComboBox = new ComboBox<>("Пациент");
    private ComboBox<Doctor> doctorComboBox = new ComboBox<>("Врач");
    private DateField dateOfCreationField = new DateField("Дата создания");
    private DateField expirationDateField = new DateField("Срок действия");
    private ComboBox<Priority> priorityComboBox = new ComboBox<>("Приоритет");
    private Binder<Recipe> binder = new Binder<>();
    private RecipeWindowGrid previousWindow;
    private Recipe selectedRecipe;

    public RecipeWindowEdit(String button, Recipe recipe, RecipeWindowGrid previousWindow,
                            List<Patient> patienceList, List<Doctor> doctorList) {
        this.selectedRecipe = recipe;
        this.previousWindow = previousWindow;
        if (button.equals("edit")) {
            setBinder();
            addValues(recipe);
        }
        setItemsToCheckBox(patienceList, doctorList);
        VerticalLayout layout = createVerticalLayout(button);
        setContent(layout);
    }

    private VerticalLayout createVerticalLayout(String button) {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponentsAndExpand(descriptionField);
        layout.addComponentsAndExpand(patientComboBox);
        layout.addComponentsAndExpand(doctorComboBox);
        layout.addComponent(dateOfCreationField);
        layout.addComponent(expirationDateField);
        layout.addComponent(priorityComboBox);
        layout.addComponent(getButtonLayout(button));
        return layout;
    }

    private void setBinder() {
        binder.bind(descriptionField, Recipe::getDescription, Recipe::setDescription);
        binder.bind(patientComboBox, Recipe::getPatient, Recipe::setPatient);
        binder.bind(doctorComboBox, Recipe::getDoctor, Recipe::setDoctor);
        binder.bind(dateOfCreationField, Recipe::getDateOfCreation, Recipe::setDateOfCreation);
        binder.bind(priorityComboBox, Recipe::getPriority, Recipe::setPriority);
        binder.bind(expirationDateField, Recipe::getExpirationDate, Recipe::setExpirationDate);
    }

    private void addValues(Recipe recipe) {
        binder.readBean(recipe);
    }

    private void setItemsToCheckBox(List<Patient> patienceList, List<Doctor> doctorList) {
        priorityComboBox.setItems(Priority.values());
        patientComboBox.setItems(patienceList);
        doctorComboBox.setItems(doctorList);
    }

    private HorizontalLayout getButtonLayout(String button) {
        HorizontalLayout layout = new HorizontalLayout();
        Button okButton = createOKButton(button);
        Button undoButton = createUNDOButton();
        layout.addComponent(okButton);
        layout.addComponent(undoButton);
        return layout;
    }

    private Button createOKButton(String button) {
        Button okButton = new Button("ОК");
        okButton.addClickListener(clickEvent -> {
            if (binder.isValid()) {
                if (dateOfCreationField.getValue().isAfter(expirationDateField.getValue())){
                    Notification.show("Дата создания не может быть позже срока действия", Notification.Type.ERROR_MESSAGE);
                } else {
                    if (button.equals("edit")) {
                        updateRecipe();
                    } else {
                        createRecipe();
                    }
                    previousWindow.updateDataRecipe();
                }
            }
        });
        return okButton;
    }

    private Button createUNDOButton() {
        Button undoButton = new Button("Отменить");
        undoButton.addClickListener(clickEvent -> this.close());
        return undoButton;
    }

    private void updateRecipe() {
        DAO<Recipe, Long> dao = new RecipeDAO(MainUI.SESSION_FACTORY);
        selectedRecipe.setExpirationDate(expirationDateField.getValue());
        selectedRecipe.setDateOfCreation(dateOfCreationField.getValue());
        selectedRecipe.setDescription(descriptionField.getValue());
        if (patientComboBox.getValue() != null) {
            selectedRecipe.setPatient(patientComboBox.getValue());
        }
        if (doctorComboBox.getValue() != null) {
            selectedRecipe.setDoctor(doctorComboBox.getValue());
        }
        if (priorityComboBox.getValue() != null) {
            selectedRecipe.setPriority(priorityComboBox.getValue());
        }
        if (!dao.update(selectedRecipe)) {
            Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
        } else {
            Notification.show("Данные обновлены", Notification.Type.TRAY_NOTIFICATION);
            this.close();
        }
    }

    private void createRecipe() {
        try {
            DAO<Recipe, Long> dao = new RecipeDAO(MainUI.SESSION_FACTORY);
            Recipe recipe = new Recipe();
            recipe.setId(1L);
            recipe.setDescription(descriptionField.getValue());
            recipe.setDoctor(doctorComboBox.getValue());
            recipe.setPatient(patientComboBox.getValue());
            recipe.setDateOfCreation(dateOfCreationField.getValue());
            recipe.setExpirationDate(expirationDateField.getValue());
            recipe.setPriority(priorityComboBox.getValue());
            if (!dao.create(recipe)) {
                Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
            } else {
                Notification.show("Создано", Notification.Type.TRAY_NOTIFICATION);
                this.close();
            }
        } catch (NullPointerException exception) {
            Notification.show("Все поля обязательны к заполнению", Notification.Type.ERROR_MESSAGE);
        }

    }
}
