package com.haulmont.testtask.ui.recipeWindows;

import com.haulmont.testtask.dao.DoctorDAO;
import com.haulmont.testtask.dao.PatientDAO;
import com.haulmont.testtask.dao.RecipeDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Doctor;
import com.haulmont.testtask.model.Patient;
import com.haulmont.testtask.model.Priority;
import com.haulmont.testtask.model.Recipe;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;

public class RecipeWindowGrid extends Window {

    private Grid<Recipe> grid = new Grid<>();
    private Recipe selectedRecipe;
    private VerticalLayout layout = new VerticalLayout();
    private MainUI mainUI;
    private TextField descriptionFilterField = new TextField("Описание");
    private ComboBox<Patient> patientComboBox = new ComboBox<>("Пациенты");
    private ComboBox<Priority> priorityComboBox = new ComboBox<>("Приоритет");

    public RecipeWindowGrid(MainUI mainUI) {
        this.mainUI = mainUI;
        initGrid();
        updateDataRecipe();
        layout.addComponent(createMenuButtonsLayout());
        layout.addComponent(createAndGetFilterPanel());
        layout.addComponent(getButtonLayout());
        layout.addComponentsAndExpand(grid);
        setContent(layout);
    }

    private void initGrid() {
        grid.addColumn(Recipe::getDescription).setCaption("Описание");
        grid.addColumn(recipe -> recipe.getDoctor().toString()).setCaption("Доктор");
        grid.addColumn(recipe -> recipe.getPatient().toString()).setCaption("Пациент");
        grid.addColumn(Recipe::getDateOfCreation).setCaption("Дата создания");
        grid.addColumn(Recipe::getExpirationDate).setCaption("Срок действия");
        grid.addColumn(Recipe::getPriority).setCaption("Приоритет");
        grid.setVisible(true);
        grid.setSizeFull();
        grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> selectedRecipe = valueChangeEvent.getValue());
    }

    public void updateDataRecipe() {
        RecipeDAO dao = new RecipeDAO(MainUI.SESSION_FACTORY);
        List<Recipe> list = dao.getAll();
        grid.setItems(list);
    }

    private HorizontalLayout createMenuButtonsLayout() {
        return mainUI.createMenuHorizontalLayout();
    }

    private Panel createAndGetFilterPanel() {
        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.addComponent(descriptionFilterField);
        setItemsToComboBox();
        filterLayout.addComponent(patientComboBox);
        filterLayout.addComponent(priorityComboBox);
        Button filterButton = createFilterButton();
        filterLayout.addComponent(filterButton);
        filterLayout.setComponentAlignment(filterButton, Alignment.BOTTOM_CENTER);
        Panel panel = new Panel("Фильтр");
        panel.setContent(filterLayout);
        panel.setHeight("125px");
        panel.setWidth("750px");
        return panel;
    }

    private void setItemsToComboBox() {
        DAO<Patient, Long> patientDAO = new PatientDAO(MainUI.SESSION_FACTORY);
        patientComboBox.setItems(patientDAO.getAll());
        priorityComboBox.setItems(Priority.values());
    }

    private HorizontalLayout getButtonLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        Button addButton = createAddButton();
        Button editButton = createEditButton();
        Button deleteButton = createDeleteButton();
        layout.addComponent(addButton);
        layout.addComponent(editButton);
        layout.addComponent(deleteButton);
        return layout;
    }

    private Button createAddButton() {
        Button addButton = new Button("Добавить");
        DAO<Doctor, Long> daoDoctor = new DoctorDAO(MainUI.SESSION_FACTORY);
        DAO<Patient, Long> daoPatient = new PatientDAO(MainUI.SESSION_FACTORY);
        addButton.addClickListener(e -> {
            RecipeWindowEdit window = new RecipeWindowEdit("add", null, this, daoPatient.getAll(), daoDoctor.getAll());
            setDefaultWindowSettings(window);
            UI.getCurrent().addWindow(window);
        });
        return addButton;
    }

    private Button createEditButton() {
        Button editButton = new Button("Изменить");
        DAO<Doctor, Long> daoDoctor = new DoctorDAO(MainUI.SESSION_FACTORY);
        DAO<Patient, Long> daoPatient = new PatientDAO(MainUI.SESSION_FACTORY);
        editButton.addClickListener(clickEvent -> {
            if (selectedRecipe != null) {
                RecipeWindowEdit window = new RecipeWindowEdit("edit", selectedRecipe, this, daoPatient.getAll(), daoDoctor.getAll());
                setDefaultWindowSettings(window);
                UI.getCurrent().addWindow(window);
            } else {
                Notification.show("Выберите строку для её изменения", Notification.Type.WARNING_MESSAGE);
            }
        });
        return editButton;
    }

    private Button createDeleteButton() {
        Button deleteButton = new Button("Удалить");
        deleteButton.addClickListener(clickEvent -> {
            if (selectedRecipe != null) {
                DAO<Recipe, Long> dao = new RecipeDAO(MainUI.SESSION_FACTORY);
                if (dao.delete(selectedRecipe)) {
                    selectedRecipe = null;
                    updateDataRecipe();
                    Notification.show("Удалено", Notification.Type.TRAY_NOTIFICATION);
                } else {
                    Notification.show("Невозможно удалить:что-то пошло не так \n нажмите чтобы убрать", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                Notification.show("Выберите строку для её удаления", Notification.Type.WARNING_MESSAGE);
            }
        });
        return deleteButton;
    }

    private Button createFilterButton() {
        Button filterButton = new Button("Применить");
        filterButton.addClickListener(clickEvent -> {
            DAO<Recipe, Long> recipeDAO = new RecipeDAO(MainUI.SESSION_FACTORY);
            List<Recipe> list = recipeDAO.getAll();
            List<Recipe> filterList = new ArrayList<>();
            for (Recipe object : list) {
                String description = descriptionFilterField.getValue().toUpperCase();
                Patient patient = patientComboBox.getValue();
                Priority priority = priorityComboBox.getValue();
                if (description.equals("") && patient == null && priority == null) {
                    filterList.addAll(list);
                    break;
                }
                if (checkDescriptionField(object, description) ||
                        checkPatientField(object, patient) ||
                        checkPriorityField(object, priority)) {
                    continue;
                }
                filterList.add(object);
            }
            updateDataRecipe(filterList);
        });
        return filterButton;
    }

    private boolean checkPriorityField(Recipe object, Priority priority) {
        return priority != null && !object.getPriority().equals(priority);
    }

    private boolean checkDescriptionField(Recipe object, String description) {
        return !description.equals("") && !object.getDescription().contains(description);
    }

    private boolean checkPatientField(Recipe object, Patient patient) {
        return patient != null && !object.getPatient().equals(patient);
    }

    private void updateDataRecipe(List<Recipe> recipes) {
        grid.setItems(recipes);
    }

    private void setDefaultWindowSettings(Window window) {
        window.setPosition(800, 200);
        window.setResizable(false);
        window.setDraggable(true);
        window.setWidth("300px");
        window.setHeight("500px");
    }

    public VerticalLayout getLayout() {
        return layout;
    }
}
