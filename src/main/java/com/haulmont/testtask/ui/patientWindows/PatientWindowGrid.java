package com.haulmont.testtask.ui.patientWindows;

import com.haulmont.testtask.dao.PatientDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Patient;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.ui.*;

import java.util.List;

public class PatientWindowGrid extends Window {

    private Patient selected = null;
    private Grid<Patient> grid = new Grid<>();
    private VerticalLayout layout = new VerticalLayout();
    private MainUI mainUI;
    private DAO<Patient, Long> dao = new PatientDAO(MainUI.SESSION_FACTORY);

    public PatientWindowGrid(MainUI mainUI) {
        this.mainUI = mainUI;
        initGrid();
        updateDataPatient();
        layout.addComponent(createMenuButtonsLayout());
        layout.addComponent(getButtonLayout());
        layout.addComponentsAndExpand(grid);
        setContent(layout);
    }

    public VerticalLayout getLayout() {
        return layout;
    }

    private void initGrid() {
        grid.addColumn(Patient::getSurname).setCaption("Фамилия");
        grid.addColumn(Patient::getName).setCaption("Имя");
        grid.addColumn(Patient::getPatronymic).setCaption("Отчество");
        grid.addColumn(Patient::getPhoneNumber).setCaption("Телефон");
        grid.setVisible(true);
        grid.setSizeFull();
        grid.asSingleSelect().addValueChangeListener(valueChangeEvent -> selected = valueChangeEvent.getValue());
    }

    public void updateDataPatient() {
        List<Patient> list = dao.getAll();
        grid.setItems(list);
    }

    private HorizontalLayout createMenuButtonsLayout() {
        return mainUI.createMenuHorizontalLayout();
    }

    private HorizontalLayout getButtonLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        Button addButton = createAddButton();
        Button editButton = createEditButton();
        Button deleteButton = createDeleteButton();
        layout.addComponent(addButton);
        layout.addComponent(editButton);
        layout.addComponent(deleteButton);
        return layout;
    }

    private Button createAddButton() {
        Button addButton = new Button("Добавить");
        addButton.addClickListener(e -> {
            PatientWindowEdit window = new PatientWindowEdit("add", null, this);
            setDefaultWindowSettings(window);
            UI.getCurrent().addWindow(window);
        });
        return addButton;
    }

    private Button createEditButton() {
        Button editButton = new Button("Изменить");
        editButton.addClickListener(clickEvent -> {
            if (selected != null) {
                PatientWindowEdit window = new PatientWindowEdit("edit", selected, this);
                setDefaultWindowSettings(window);
                UI.getCurrent().addWindow(window);
            } else {
                Notification.show("Выберите строку для её изменения", Notification.Type.WARNING_MESSAGE);
            }
        });
        return editButton;
    }

    private Button createDeleteButton() {
        Button deleteButton = new Button("Удалить");
        deleteButton.addClickListener(clickEvent -> {
            if (selected != null) {
                if (dao.delete(selected)) {
                    selected = null;
                    updateDataPatient();
                    Notification.show("Удалено", Notification.Type.TRAY_NOTIFICATION);
                } else {
                    Notification.show("Невозможно удалить:существует рецепт", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                Notification.show("Выберите строку для её удаления", Notification.Type.WARNING_MESSAGE);
            }
        });
        return deleteButton;
    }

    private void setDefaultWindowSettings(Window window) {
        window.setPosition(800, 200);
        window.setResizable(false);
        window.setWidth("300px");
        window.setHeight("400px");
    }

}