package com.haulmont.testtask.ui.patientWindows;

import com.haulmont.testtask.dao.PatientDAO;
import com.haulmont.testtask.interfaces.DAO;
import com.haulmont.testtask.model.Patient;
import com.haulmont.testtask.ui.MainUI;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;

public class PatientWindowEdit extends Window {

    private DAO<Patient, Long> dao = new PatientDAO(MainUI.SESSION_FACTORY);
    private TextField surnameTextField = new TextField("Фамилия");
    private TextField nameTextField = new TextField("Имя");
    private TextField patronymicTextField = new TextField("Отчество");
    private TextField phoneNumberTextField = new TextField("Телефон");
    private Binder<Patient> binder = new Binder<>();
    private PatientWindowGrid previousWindow;
    private Patient selectedPatient;

    public PatientWindowEdit(String button, Patient selectedPatient, PatientWindowGrid previousWindow) {
        this.selectedPatient = selectedPatient;
        this.previousWindow = previousWindow;
        initBinder(button, selectedPatient);
        VerticalLayout layout = createVerticalLayout(button);
        setContent(layout);
    }

    private void initBinder(String button, Patient selectedPatient) {
        if (button.equals("edit")) {
            setBinder();
            addValues(selectedPatient);
        }
    }

    private void setBinder() {
        binder.bind(surnameTextField, Patient::getSurname, Patient::setSurname);
        binder.bind(nameTextField, Patient::getName, Patient::setName);
        binder.bind(patronymicTextField, Patient::getPatronymic, Patient::setPatronymic);
        binder.bind(phoneNumberTextField, Patient::getPhoneNumber, Patient::setPhoneNumber);
    }

    private void addValues(Patient patient) {
        binder.readBean(patient);
    }

    private VerticalLayout createVerticalLayout(String button) {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(surnameTextField);
        layout.addComponent(nameTextField);
        layout.addComponent(patronymicTextField);
        layout.addComponent(phoneNumberTextField);
        layout.addComponent(getButtonLayout(button));
        return layout;
    }

    private HorizontalLayout getButtonLayout(String button) {
        HorizontalLayout layout = new HorizontalLayout();
        Button okButton = createOKButton(button);
        Button undoButton = createUNDOButton();
        layout.addComponent(okButton);
        layout.addComponent(undoButton);
        return layout;
    }

    private Button createOKButton(String button) {
        Button okButton = new Button("ОК");
        okButton.addClickListener(clickEvent -> {
            if (binder.isValid()) {
                if (button.equals("edit")) {
                    try {
                        updatePatient();
                    } catch (ValidationException e) {
                        e.printStackTrace();
                    }
                } else {
                    createPatient();
                }
                previousWindow.updateDataPatient();
            }
        });
        return okButton;
    }

    private Button createUNDOButton() {
        Button undoButton = new Button("Отменить");
        undoButton.addClickListener(clickEvent -> this.close());
        return undoButton;
    }

    private void updatePatient() throws ValidationException {
        binder.writeBean(selectedPatient);
        dao.update(selectedPatient);
        if (!dao.update(selectedPatient)){
            Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
        } else {
            Notification.show("Данные обновлены", Notification.Type.TRAY_NOTIFICATION);
            this.close();
        }
    }

    private void createPatient() {
        try {
            Patient patient = new Patient();
            patient.setId(1L);
            patient.setSurname(surnameTextField.getValue());
            patient.setName(nameTextField.getValue());
            patient.setPatronymic(patronymicTextField.getValue());
            patient.setPhoneNumber(phoneNumberTextField.getValue());
            if (!dao.create(patient)){
                Notification.show("Проверьте правильность введённых данных", Notification.Type.ERROR_MESSAGE);
            } else {
                Notification.show("Создано", Notification.Type.TRAY_NOTIFICATION);
                this.close();
            }
        } catch (NullPointerException exception){
            Notification.show("Все поля обязательны к заполнению", Notification.Type.ERROR_MESSAGE);
        }
    }

}
