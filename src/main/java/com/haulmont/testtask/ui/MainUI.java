package com.haulmont.testtask.ui;

import com.haulmont.testtask.ui.doctorWindows.DoctorWindowGrid;
import com.haulmont.testtask.ui.patientWindows.PatientWindowGrid;
import com.haulmont.testtask.ui.recipeWindows.RecipeWindowGrid;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.UI;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MainUI extends UI {

    public static SessionFactory SESSION_FACTORY = new Configuration().configure().buildSessionFactory();

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(new DoctorWindowGrid(this).getLayout());
    }

    public HorizontalLayout createMenuHorizontalLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        NativeButton doctorButton = createDoctorMenuButton();
        NativeButton patientButton = createPatientMenuButton();
        NativeButton recipeButton = createRecipeMenuButton();
        layout.addComponent(doctorButton);
        layout.addComponent(patientButton);
        layout.addComponent(recipeButton);
        return layout;
    }

    private NativeButton createPatientMenuButton() {
        NativeButton patientButton = new NativeButton("Пациенты");
        patientButton.addClickListener(clickEvent -> {
           getWindows().forEach(this::removeWindow);
           setContent(new PatientWindowGrid(this).getLayout());
        });
        return patientButton;
    }

    private NativeButton createDoctorMenuButton() {
        NativeButton doctorButton = new NativeButton("Доктора");
        doctorButton.addClickListener(clickEvent -> {
            getWindows().forEach(this::removeWindow);
            setContent(new DoctorWindowGrid(this).getLayout());
        });
        return doctorButton;
    }

    private NativeButton createRecipeMenuButton() {
        NativeButton recipeButton = new NativeButton("Рецепты");
        recipeButton.addClickListener(clickEvent -> {
            getWindows().forEach(this::removeWindow);
            setContent(new RecipeWindowGrid(this).getLayout());
        });
        return recipeButton;
    }
}
